; to avoid (setq-default TeX-master nil)
; http://lists.gnu.org/archive/html/emacs-orgmode/2012-03/msg00285.html
(require 'org-protocol)

(add-to-list 'auto-mode-alist '("\\.\\(org\\|org_archive\\|txt\\)$" . org-mode))

(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cb" 'org-iswitchb)

(add-hook 'org-mode-hook 'yas/minor-mode-on)

(setq
 org-modules '(org-info org-jsinfo org-export)
 org-log-done (quote time)
 org-reverse-note-order t
 org-deadline-warning-days 14
 org-hide-leading-stars t
 org-use-fast-todo-selection t
 org-use-fast-tag-selection 'auto
 org-fast-tag-selection-single-key t
 org-special-ctrl-a/e t
 org-special-ctrl-k t
 org-M-RET-may-split-line nil
 org-time-clocksum-format "%02d:%02d"
 org-clock-into-drawer "CLOCK"
 org-clock-persist t
 org-clock-in-switch-to-state "STARTED" ;; Change task state to STARTED when clocking in
 org-read-date-prefer-future nil
 org-log-into-drawer "LOGBOOK"
 org-completion-use-ido t
 org-tags-exclude-from-inheritance nil
 org-goto-interface 'outline-path-completion
 org-cycle-separator-lines 0
 org-cycle-include-plain-lists t
 org-blank-before-new-entry '((heading) (plain-list-item))

 org-list-demote-modify-bullet
 '(("+" . "-")
   ("*" . "-")
   ("1." . "-")
   ("1)" . "-"))
 
 ;; Showing context
 org-show-hierarchy-above '((default . t))
 org-show-following-heading '((default . t))
 org-show-siblings '((default . t))
 
 ;; Agenda Files and Agenda Settings
 org-agenda-files nil
 org-agenda-include-all-todo t
 org-agenda-window-setup 'current-window
 org-agenda-restore-windows-after-quit nil
 org-agenda-ndays 1
 org-agenda-skip-deadline-if-done t
 org-agenda-skip-scheduled-if-done t
 org-agenda-show-all-dates t
 org-agenda-start-on-weekday 1
 org-agenda-todo-ignore-with-date t
 org-stuck-projects '("LEVEL=2+project/-DONE" ("NEXT" "PENDING") ("single") "")

 ;; diary
 org-agenda-include-diary nil
 org-agenda-diary-file "~/.diary.org"
 diary-file "~/.diary.org"
 calendar-mark-diary-entries-flag t
 calendar-view-diary-initially-flag t
 mark-diary-entries-in-calendar t
 view-diary-entries-initially t
 
 ;; TODO keywords
 org-todo-keywords
 '((sequence "TODO(t)" "STARTED(s!)" "|" "DONE(d!/!)")
   (sequence "WAITING(w@/!)" "SOMEDAY(S!)" "|" "CANCELLED(c@/!)")
   ; (sequence "QUOTATION(q!)" "QUOTED(Q!)" "|" "APPROVED(A@)" "EXPIRED(E@)" "REJECTED(R@)")
   ; (sequence "OPENPO(!)" "|" "CLOSEDPO(@)")
   ; (sequence "PROJECT(P@)" "|" "PROJDONE(D@)")
   )

 org-todo-state-tags-triggers
 '(("CANCELLED" ("CANCELLED" . t))
   ("WAITING" ("WAITING" . t) ("NEXT"))
   ("SOMEDAY" ("WAITING" . t))
   (done ("NEXT") ("WAITING"))
   ("TODO" ("WAITING") ("CANCELLED"))
   ("STARTED" ("WAITING"))
   ("PROJECT" ("CANCELLED") ("PROJECT" . t)))
 
 
; org-agenda-files (list "~/org/agenda.org")
;;  org-agenda-files 
;;  (append
;;   (directory-files pmade-org-active-clients t "\\.org$")
;;   (directory-files pmade-org-general-files  t "\\.org$"))

 org-file-apps
 '((auto-mode . emacs)
   ("\\.x?html?\\'" . default))
 )
 
;; ensure this variable is defined
(unless (boundp 'org-babel-default-header-args:sh)
  (setq org-babel-default-header-args:sh '()))

;; I don't want to be prompted on every code block evaluation
(setq org-confirm-babel-evaluate nil)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((python t)
   (org t)
   (sh t)))

;;(require 'htmlize)
;;(setq htmlize-convert-nonascii-to-entities t)
;;(setq htmlize-html-charset "utf-8")
