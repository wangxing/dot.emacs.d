(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(custom-enabled-themes (quote (light-blue)))
 '(custom-safe-themes
   (quote
    ("0d2cfe3ecfeb3c29615b514fc974235c248718d125324e8a8a4906b481ee8d7b" "575b87c1b79f67e5bb70c8a2f0dc78d959354592620f58bf6f6910043b680521" "b5ec240f00d43bb1f69120a293d51f33f63f5708bfb3b199e3a78adcdc6f3ecd" "bf83b2dd1f23a47f6c147e3da692ea6fb3205c94e0cfbf318a1282dc5044f598" default)))
 '(display-time-mode t)
 '(global-linum-mode t)
 '(inhibit-startup-screen t)
 '(linum-format "%d ")
 '(scroll-bar-mode nil)
 '(size-indication-mode t)
 '(tool-bar-mode nil)
 '(tooltip-mode nil)
 '(window-numbering-mode t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Droid Sans Mono" :foundry "outline" :slant normal :weight normal :height 113 :width normal)))))
