;;; TRAMP for remote editing
(require 'tramp)
(setq tramp-default-method "ssh")
