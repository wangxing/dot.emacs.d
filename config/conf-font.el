;;; English font
(set-face-attribute
 'default nil :font "Droid Sans Mono 11")

;;; Chinese font
(if (display-graphic-p)
    (dolist (charset '(kana han symbol cjk-misc bopomofo))
      (set-fontset-font (frame-parameter nil 'font)
			charset
			(font-spec :family "Microsoft Yahei" :size 14))))
