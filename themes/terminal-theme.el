(deftheme terminal
  "Created 2012-01-13.")

(custom-theme-set-faces
 'terminal
 '(default ((t (:background "black" :foreground "grey70"))))
 '(cursor ((t (:foreground "red" :background "red"))))
 '(highlight ((t (:background "grey20")))) 
 '(font-lock-variable-name-face ((t (:foreground "blue"))))
 '(font-lock-string-face ((t (:foreground "green"))))
 '(font-lock-builtin-face ((t (:foreground "blue" :bold t))))
 '(font-lock-constant-face ((t (:foreground "cyan"))))
 '(font-lock-type-face ((t (:foreground "green" :bold t))))
; '(show-paren-match ((t (:background "red" :foreground "yellow" :bold t))))
 '(show-paren-match ((t (:background "#DA44FF" :foreground "#F6CCFF" ))))
 '(show-paren-mismatch ((((class color)) (:background "red" :foreground "white")))) 
 '(font-lock-comment-delimiter-face ((t (:foreground "white"))))
 '(font-lock-comment-face ((t (:italic t :foreground "magenta"))))
 )

(provide-theme 'terminal)
