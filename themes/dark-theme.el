(deftheme dark
  "Created 2012-01-13.")

(custom-theme-set-faces
 'dark
 '(default ((t (:family "DejaVu Sans Mono" :foundry "unknown" :width normal :height 158 :weight normal :slant normal :underline nil :overline nil :strike-through nil :box nil :inverse-video nil :foreground "grey70" :background "black" :stipple nil))))
 '(cursor ((t (:foreground "red" :background "red"))))
 '(region ((t :background "#505C74")))
 '(highlight ((t (:background "grey20"))))
 '(font-lock-comment-face ((t (:italic t :foreground "Firebrick" :background "black"))))
 '(font-lock-string-face ((t (:foreground "#A5F26E"))))
 '(font-lock-keyword-face ((t (:bold t :foreground "#CC7832"))))
 '(font-lock-warning-face ((t (:underline "red"))))
 '(font-lock-constant-face ((t (:foreground "#6BCFF7"))))
 '(font-lock-type-face ((t (:bold t :foreground "#8888ff"))))
 '(font-lock-variable-name-face ((t (:foreground "grey85"))))
 '(font-lock-function-name-face ((t (:bold t :foreground "#E8BF6A"))))
 '(font-lock-builtin-face ((t (:foreground "#59ACC2"))))
 '(font-lock-preprocessor-face ((t (:background "#202020"))))
 '(show-paren-match ((t (:background "#DA44FF" :foreground "#F6CCFF" ))))
 '(show-paren-mismatch ((((class color)) (:background "red" :foreground "white"))))

 ;; Outline Mode and Org-Mode
 '(outline-1 ((t (:foreground "#D6B163" :bold t))))
 '(outline-2 ((t (:foreground "#A5F26E" :bold t))))
 '(outline-3 ((t (:foreground "#B150E7" :bold nil))))
 '(outline-4 ((t (:foreground "#529DB0" :bold nil))))
 '(outline-5 ((t (:foreground "#CC7832" :bold nil))))
 '(org-level-1 ((t (:inherit outline-1))))
 '(org-level-2 ((t (:inherit outline-2))))
 '(org-level-3 ((t (:inherit outline-3))))
 '(org-level-4 ((t (:inherit outline-4))))
 '(org-level-5 ((t (:inherit outline-5))))
 '(org-agenda-date ((t (:inherit font-lock-type-face))))
 '(org-agenda-date-weekend ((t (:inherit org-agenda-date))))
 '(org-scheduled-today ((t (:foreground "#ff6ab9" :italic t))))
 '(org-scheduled-previously ((t (:foreground "#d74b4b"))))
 '(org-upcoming-deadline ((t (:foreground "#d6ff9c"))))
 '(org-warning ((t (:foreground "#8f6aff" :italic t))))
 '(org-date ((t (:inherit font-lock-constant-face))))
 '(org-tag ((t (:inherit font-lock-comment-delimiter-face))))
 '(org-hide ((t (:foreground "#191919"))))
 '(org-todo ((t (:background "DarkRed" :foreground "white" :box (:line-width 1 :style released-button)))))
 '(org-done ((t (:background "DarkGreen" :foreground "white" :box (:line-width 1 :style released-button)))))
 '(org-column ((t (:background "#222222"))))
 '(org-column-title ((t (:background "DarkGreen" :foreground "white" :bold t :box (:line-width 1 :style released-button)))))

 ;; Diff Mode
 '(diff-added ((t (:foreground "#d7ffaf"))))
 '(diff-changed ((t (:foreground "#ffc28d"))))
 '(diff-removed ((t (:foreground "#ff9999"))))
 '(diff-indicator-added ((t (:background "#d7ffaf" :foreground "#000000"))))
 '(diff-indicator-chnaged ((t (:background "#ffc28d" :foreground "#000000"))))
 '(diff-indicator-removed ((t (:background "#ff9999" :foreground "#000000"))))
 '(diff-context ((t (:foreground "#888888"))))

 ;; Compilation
 '(compilation-info ((t (:inherit 'font-lock-string-face :bold t))))
 '(compilation-error ((t (:background "sienna4" :bold t))))
 '(compilation-line-number ((t (:foreground "#FF6666" :bold t))))
 '(flymake-errline ((t :underline "red")))
 '(flymake-warnline ((t :underline "green")))

 ;; nXML
 '(nxml-element-colon-face    ((t (:bold t :foreground "#92D229"))))
 '(nxml-element-prefix-face    ((t (:bold t :foreground "#92D229"))))
 '(nxml-attribute-value-delimiter-face ((t (:inherit 'font-lock-string-face))))
 '(nxml-cdata-section-content-face ((t (:inherit 'font-lock-string-face))))
 '(nxml-attribute-value-face ((t (:inherit 'font-lock-string-face))))
 '(nxml-attribute-local-name-face ((t (:inherit 'font-lock-constant-face))))
 '(nxml-attribute-local-name-face ((t (:inherit 'font-lock-constant-face))))
 '(nxml-entity-ref-name-face ((t (:inherit 'font-lock-constant-face))))
 '(nxml-element-colon-face    ((t (:inherit 'font-lock-function-name-face))))
 '(nxml-element-prefix-face    ((t (:inherit 'font-lock-function-name-face))))
 '(nxml-element-local-name-face    ((t (:inherit 'font-lock-function-name-face))))
 '(nxml-tag-delimiter-face    ((t (:inherit 'font-lock-function-name-face))))
 '(nxml-tag-slash-face    ((t (:inherit 'font-lock-function-name-face))))
 '(nxml-comment-delimiter-face ((t (:inherit 'font-lock-comment-face))))
 '(nxml-comment-content-face ((t (:inherit 'font-lock-comment-face))))

 ;; ido
 '(ido-first-match ((t (:inherit 'font-lock-string-face))))
 '(ido-subdir ((t (:inherit 'font-lock-function-name-face))))

 ;; Minibuffer
 '(minibuffer-noticeable-prompt ((t (:inherit 'font-lock-builtin-face :bold t))))

 ;; Modeline and Things in the Modeline
 '(mode-line ((t (:background "grey60" :foreground "black" :height 0.8))))
 '(mode-line-inactive ((t (:inherit mode-line :height 0.8))))
 '(modeline-buffer-id ((t (:background "DarkRed" :foreground "white"))))
 '(modeline-mousable ((t (:background "DarkRed" :foreground "white"))))
 '(modeline-mousable-minor-mode ((t (:background "DarkRed" :foreground "white"))))
 '(window-number-face ((t (:foreground "#FF7777"))))

 ;; Bookmarks
 '(bm-fringe-face ((t (:foreground "#ff8e43"))))

 )

(provide-theme 'dark)
